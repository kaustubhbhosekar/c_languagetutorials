#include <stdio.h>
#include <math.h>

int main()
{
    int a = 4;
    int b = 8;
    int z;
    z = b * a;
    //b * a = z illegal

    printf("The value of z is %d\n", z);
    printf("The value of a+b is %d\n", a + b);
    printf("The value of a-b is %d\n", a - b);
    printf("The value of a*b is %d\n", a * b);
    printf("The value of a/b is %d\n", a / b);

    printf("when divided by 2 leaves a remainder of %d", 5 % 2);
    printf("value of 4 to the power 5 is  %f", pow(2, 5));
    int k = 3.0 / 9;
    printf("value of k  is  %d\n ", k);

    return 0;
}