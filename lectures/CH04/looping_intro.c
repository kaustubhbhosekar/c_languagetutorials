#include <stdio.h>

int main()
{
    //loops are used to reprat simmilar parts of code snippetes effitiently
    //while loop

    // int a;
    // scanf("%d", &a);

    // while (a < 10)
    // {
    //     printf("%d\n ", a);
    //     a++;
    // }

    //do while

    // int i = 0;
    // int n;
    // printf("enter the number of times you want to execute the loop?");
    // scanf("%d", &n);

    // do
    // {
    //     printf("the value of i is %d\n", i+1);
    //     i++;
    // } while (i <n);

    // for loop

    for (int a = 10; a ; a--)
    {
        
        if(a==5)
        {
            continue;
        }
        printf("The value of a is %d \n", a);
    }

    return 0;
}