#include <stdio.h>

int main()
{
    // intiger increment
    // int i = 34;
    // int *ptr = &i;
    // printf("The value of ptr is %d \n ", ptr); 6422219
    // ptr = ptr + 1;
    // // ptr++;
    // // ptr++;
    // printf("The value of ptr is %d \n ", ptr); 6422220
    // return 0;


    // // character increment
    // char i = 34;
    // char *ptr = &i;
    // printf("The value of ptr is %d \n ", ptr);
    // ptr = ptr + 1;
    // // ptr++;
    // // ptr++;
    // printf("The value of ptr is %d \n ", ptr);
    // return 0;

    // // float increment
    // float f = 3.4;
    // float *ptr = &f;
    // printf("The value of ptr is %u \n ", ptr);
    // ptr = ptr + 1;
    // // ptr++;
    // // ptr++;
    // printf("The value of ptr is %u \n ", ptr);
    // return 0;

    //intiger increment
    int i = 34;
    int *ptr = &i;
    int *ptr2 = &i;
    printf("The value of ptr is %d \n ", ptr); //6422219
    ptr++;
    ptr++;
    printf("The value of ptr is %d \n ", ptr); //6422220

    printf("substraction of 1 pointer from another is %d \n ",(*ptr2-*ptr));
    return 0;


}