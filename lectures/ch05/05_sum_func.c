#include <stdio.h>

int sum(int a, int b); //sum is a function which takes a and b as input parameters and returns an int value.
int main()
{
    int c = sum(2, 15);
    printf("The value of c is %d\n", c);
    return 0;
}

int sum(int a, int b)
{
    int result;
    result = a + b;
    return result;
}