#include <stdio.h>
int factorial(int x);
int main()
{
    int a = 5;
    printf("The value of a is %d", a);
    a = factorial(a);
    printf("The value of a is %d", a);

    return 0;
}

int factorial(int x)
{
    if (x == 1 || x == 0)
    {
        return 1;
    }
    else
    {
        return factorial(x - 1) * x;
    }
}