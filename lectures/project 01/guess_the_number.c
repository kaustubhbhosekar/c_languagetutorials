#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    int number, guess, nguesses = 1;
    srand(time(0));
    number = rand() % 100 + 1; // generates random number in between 1 - 100;
    // printf("The number is %d", number);
    //keep running the loop until the number is guessed
    do
    {
        printf("Guess the number between 1 - 100\n");
        scanf("%d", &guess);
        if (guess > number)
        {
            printf("The number is lower then the number guessed\n");
            nguesses++;
        }
        else if (number > guess)
        {
            printf("The number is higher then the number guessed\n");
            nguesses++;
        }
        else
        {
            printf("You guessed the number correctly in %d attempts\n", nguesses);
        }

    } while (number != guess);

    return 0;
}