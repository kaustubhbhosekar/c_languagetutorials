#include <stdio.h>

int main()
{
    int a = 4;
    // int b = 8.5; // not recomended as it is not an intiger
    float b = 8.5;
    char c = 'u';
    int d = 45;
    printf("This value of a is %d \n ", a);
    printf("This value of a is %f \n ", b);
    printf("This value of a is %c \n ", c);
    printf("This value of a +d  is %d \n ", a + d);
    return 0;
}