#include <stdio.h> 

int main()
{
    int a =6;
    printf("The address of the variable a is %d", &a);
    int *ptr;
    ptr = &a;
    printf("The address of the variable a is %d", ptr);
    printf("The address of the variable a is %d", *ptr);
    return 0;
}