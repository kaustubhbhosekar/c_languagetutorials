#include <stdio.h>

void swap(int *a, int *b);
void wrong_swap(int a, int b);
int main()
{
    int x = 3;
    int y = 4;
    printf("The value of x and y before swapping by using call by value is %d and %d \n", x, y);
    wrong_swap(x, y);
    printf("The value of x and y after swapping by using call by value is %d and %d \n", x, y);


    printf("The value of x and y before swapping by using call by refrence is %d and %d \n", x, y);
    swap(&x, &y);
    printf("The value of x and y after swapping by using call by refrence is %d and %d \n", x, y);
    return 0;
}

void wrong_swap(int a, int b)
{
    int temp;
    temp = a;
    a = b;
    b = temp;
}

void swap(int *a, int *b)
{
    int temp;
    temp = *a;
    *a = *b;
    *b = temp;
}