#include <stdio.h>

int main()
{
    float tax = 0, income;
    printf("AEnter your income\n");
    scanf("%f", &income);

    if (income >= 250000 && income <= 500000)
    {
        tax = tax + 0.05 * (income - 250000);
        printf("tax in block 1 %d", tax);
    }

    if (income >= 500000 && income <= 1000000)
    {
        tax = tax + 0.10 * (income - 500000);
        printf("tax in block 2 %d", tax);
    }

    if (income >= 1000000)
    {
        tax = tax + 0.20 * (income - 1000000);
        printf("tax in block 3 %d", tax);
    }

    printf("Your net income tax to be paid by 26 th of this month is %f", tax);

    return 0;
}